// MIT License
//
// Copyright (c) 2021-2023 Tobias Pfeiffer
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

use super::*;

pub fn run(
	reader: impl io::BufRead,
	mut writer: impl io::Write
) {
	let mut i = 0;

	for line in reader.lines() {
		let msg = match line.and_then(|line| serde_json::from_str(&line)
			.map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e)))
		{
			Ok(v) => v,
			Err(e) => {
				eprintln!("error: failed to parse message: {e}");
				std::process::exit(1);
			}
		};

		if let cargo::CargoMessage::Suite(cargo::CargoTestReportSuite::Ok(v) | cargo::CargoTestReportSuite::Failed(v)) = msg {
			if let Err(e) = write!(&mut writer,
								   r#"passed{{suite={0}}}: {1}
failed{{suite={0}}}: {2}
allowed_fail{{suite={0}}}: {3}
ignored{{suite={0}}}: {4}
measured{{suite={0}}}: {5}
filtered_out{{suite={0}}}: {6}
exec_time{{suite={0}}}: {7}
"#, format!("cargo test #{i}"), v.passed, v.failed, v.allowed_fail, v.ignored, v.measured, v.filtered_out, v.exec_time) {
				eprintln!("error: failed to generate report: {e:?}");
				std::process::exit(1);
			}

			i += 1;
		}
	}

	eprintln!("  \x1b[32;1mGenerating\x1b[0m OpenMetrics report");
}