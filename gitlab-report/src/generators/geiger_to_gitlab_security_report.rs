// MIT License
//
// Copyright (c) 2021-2023 Tobias Pfeiffer
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

use super::*;


pub fn run(
	format: gitlab_security_report::ScanType,
	reader: impl io::BufRead,
	writer: impl io::Write
) {
	let mut report = gitlab_security_report::Report {
		version:        "15.0.6".to_string(),
		scan:           gitlab_security_report::Scan {
			start_time: get_time(),
			end_time:   "".to_string(),
			r#type:     format,
			status:     gitlab_security_report::ScanStatus::Success,
			messages:   Vec::new(),
			analyzer:   gitlab_security_report::ScanAnalyzer {
				id:      "gitlab-report".to_string(),
				name:    "gitlab-report".to_string(),
				url:     Some("https://crates.io/crates/gitlab-report".to_string()),
				vendor:  "Tobias Pfeiffer".to_string(),
				version: env!("CARGO_PKG_VERSION").to_string(),
			},
			scanner:    gitlab_security_report::ScanScanner {
				id:      "cargo-geiger".to_string(),
				name:    "Geiger".to_string(),
				url:     Some("https://crates.io/crates/cargo-geiger".to_string()),
				vendor:  "Rust Secure Code Working Group".to_string(),
				version: "?".to_string(),
			},
		},
		vulnerabilities:  Vec::new(),
		remediations:     Vec::new(),
		dependency_files: Vec::new()
	};
	let scanner    = gitlab_security_report::VulnerabilityScanner { id: "cargo_geiger".to_string(), name: "Cargo Geiger".to_string() };
	let geiger     = match serde_json::from_reader::<_, geiger::Report>(reader) {
		Ok(v) => v,
		Err(e) => {
			eprintln!("error: failed to parse report: {e}");
			std::process::exit(1);
		}
	};

	for package in geiger.packages {
		let unsafe_ = package.unsafety.used.functions.unsafe_ + package.unsafety.unused.functions.unsafe_
					  + package.unsafety.used.exprs.unsafe_ + package.unsafety.unused.exprs.unsafe_
					  + package.unsafety.used.item_impls.unsafe_ + package.unsafety.unused.item_impls.unsafe_
					  + package.unsafety.used.item_traits.unsafe_ + package.unsafety.unused.item_traits.unsafe_
					  + package.unsafety.used.methods.unsafe_ + package.unsafety.unused.methods.unsafe_;

		if package.unsafety.forbids_unsafe && unsafe_ == 0 {
			continue;
		}

		report.vulnerabilities.push(gitlab_security_report::Vulnerability {
			scanner: scanner.clone(),
			..geiger_package_to_gitlab_vuln(package, format)
		});
	}

	report.scan.end_time = get_time();

	if let Err(e) = serde_json::to_writer(writer, &report) {
		eprintln!("error: failed to generate report: {e}");
	}
}

fn geiger_package_to_gitlab_vuln(package: geiger::Package, ty: gitlab_security_report::ScanType) -> gitlab_security_report::Vulnerability {
	let unsafe_used = package.unsafety.used.functions.unsafe_
					  + package.unsafety.used.exprs.unsafe_
					  + package.unsafety.used.item_impls.unsafe_
					  + package.unsafety.used.item_traits.unsafe_
					  + package.unsafety.used.methods.unsafe_;
	let unsafe_unused = package.unsafety.unused.functions.unsafe_
						+ package.unsafety.unused.exprs.unsafe_
						+ package.unsafety.unused.item_impls.unsafe_
						+ package.unsafety.unused.item_traits.unsafe_
						+ package.unsafety.unused.methods.unsafe_;

	gitlab_security_report::Vulnerability {
		category:    "Dependency Scanning".to_string(),
		severity:    Some(gitlab_security_report::VulnerabilitySeverity::Info),
		name:        Some(format!("Unsafe usage in package `{}`", package.package.id.name)),
		message:     Some(format!("Found {} `unsafe` usages in package `{}` ({} used by the build)", unsafe_used + unsafe_unused, package.package.id.name, unsafe_used)),
		description: Some(format!(r#"Cargo Geiger Report for package `{}`:
Functions: {}/{}
Expressions: {}/{}
Impls: {}/{}
Traits: {}/{}
Methods:  {}/{}
"#, package.package.id.name,
		package.unsafety.used.functions.unsafe_, package.unsafety.unused.functions.unsafe_,
		package.unsafety.used.exprs.unsafe_, package.unsafety.unused.exprs.unsafe_,
		package.unsafety.used.item_impls.unsafe_, package.unsafety.unused.item_impls.unsafe_,
		package.unsafety.used.item_traits.unsafe_, package.unsafety.unused.item_traits.unsafe_,
		package.unsafety.used.methods.unsafe_, package.unsafety.unused.methods.unsafe_)),
		confidence:  Some(gitlab_security_report::VulnerabilityConfidence::Ignore),
		identifiers: Vec::new(),
		location:   match ty {
			gitlab_security_report::ScanType::DependencyScanning => gitlab_security_report::VulnerabilityLocation::DependencyScanning {
				file:       None,
				dependency: gitlab_security_report::VulnerabilityLocationDependency {
					package:         Some(gitlab_security_report::VulnerabilityLocationDependencyPackage { name: package.package.id.name }),
					version:         Some(package.package.id.version),
					iid:             None,
					direct:          None,
					dependency_path: Vec::new()
				}
			},
			gitlab_security_report::ScanType::Sast => gitlab_security_report::VulnerabilityLocation::Sast {
				file:       None,
				start_line: None,
				end_line:   None,
				module:     None,
				item:       None
			},
			_ => unreachable!()
		},
		..Default::default()
	}
}